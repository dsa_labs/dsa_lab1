﻿#include <iostream>
#include <time.h>
#include <windows.h>

struct date {
    unsigned short year : 12; 
    unsigned short month : 4; 
    unsigned short day : 5; 
    unsigned short hour : 5; 
    unsigned short minute : 6; 
    unsigned short second : 6; 
};

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    date now;

    now.year = 2023;
    now.month = 3;
    now.day = 21;
    now.hour = 15;
    now.minute = 30;
    now.second = 15;
    std::cout << "Поточний час і дата: " << now.year << "-" << (now.month < 10 ? "0" : "") << now.month << "-" << (now.day < 10 ? "0" : "") << now.day << " " << now.hour << ":" << now.minute << ":" << now.second << "\n";
    tm now_tm;
    now_tm.tm_year = 2023;
    now_tm.tm_mon = 3;
    now_tm.tm_mday = 21;
    now_tm.tm_hour = 15;
    now_tm.tm_min = 30;
    now_tm.tm_sec = 15;
   std::cout << "Розмір власної структури: " << sizeof(now) << " байт\n";
   std::cout << "Розмір структури tm: " << sizeof(now_tm) << " байт\n";

}

