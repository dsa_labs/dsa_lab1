﻿#include <iostream>
#include <windows.h>
#include <stdio.h>

typedef union {
    float value;
    struct {
        unsigned int mantissa : 23;
        unsigned int exponent : 8;
        unsigned int sign : 1;
    } parts;
} FloatBits;

void print_bits(FloatBits fb) {
    unsigned int* bits = (unsigned int*)&fb;
    printf("Bits: ");
    for (int i = 31; i >= 0; i--) {
        printf("%d", (bits[0] >> i) & 1);
    }
    printf("\n");
}

void print_bytes(FloatBits fb) {
    unsigned char* bytes = (unsigned char*)&fb;
    printf("Bytes: ");
    for (int i = 3; i >= 0; i--) {
        printf("%02x ", bytes[i]);
    }
    printf("\n");
}

void print_parts(FloatBits fb) {
    printf("Sign: %s\n", fb.parts.sign ? "Negative" : "Positive");
    printf("Mantissa: %06x\n", fb.parts.mantissa);
    printf("Exponent: %02x\n", fb.parts.exponent);
}


int main()
{
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);

    FloatBits f;
    printf("Введіть число: ");
    scanf_s("%f", &f.value);
	print_parts(f);

    print_bits(f);
	print_bytes(f);

    printf("Змінна займає %d байт пам'яті\n", sizeof(f)); 
	return 0;
}
