﻿#include <iostream>

int main() {
	setlocale(LC_ALL, "ukr");
	signed short num;
	std::cout << "Введіть signed short: ";
	std::cin >> num;

	bool sign = (num >> 15) & 1;

	std::cout << "Знак: " << (sign ? "Від'ємний" : "Додатній") << std::endl;
	std::cout << "Значення: " << num << std::endl;

	return 0;
}