﻿#include <iostream>
#include <windows.h>

struct signed_short_struct {
	unsigned short value : 15; 
	unsigned short sign : 1;
};

union signed_short_union {
	signed short value;
	signed_short_struct signed_short;
};

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	signed short num;
	std::cout << "Введіть signed short: ";
	std::cin >> num;

	signed_short_union union_num{};
	union_num.value = num;

	std::cout << "Знак: " << (union_num.signed_short.sign ? "Від'ємний" : "Додатній") << std::endl;
	std::cout << "Значення: " << union_num.value << std::endl;

	return 0;
}